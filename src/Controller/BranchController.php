<?php

namespace App\Controller;

use App\Exception\ApiDataException;
use App\Exception\ApiDataFetchFailedException;
use App\Service\BranchService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Contracts\Service\Attribute\Required;

#[Route('/v1/branch', name: 'branch.')]
class BranchController extends AbstractController
{
	private BranchService $branchService;

	public function __construct(BranchService $branchService)
	{
		$this->branchService = $branchService;
	}

	#[Route('/list', name: 'list' ,methods: ['GET'])]
    public function getList(): Response
    {
    	try {
		    $branchs = $this->branchService->getBranchsAsArray();

	    } catch (ApiDataFetchFailedException $e) {
		    return new JsonResponse($e->getMessage(), $e->getCode());
	    }

	    return new JsonResponse($branchs, Response::HTTP_OK);
    }

	#[Route('/detail/{internalId}', name: 'detial' ,methods: ['GET'])]
	public function getDetail(string $internalId): Response
	{

		try {
			$branchs = $this->branchService->getBranchAsArray($internalId);

		} catch (ApiDataFetchFailedException | ApiDataException $e) {
			return new JsonResponse($e->getMessage(), $e->getCode());
		}

		return new JsonResponse($branchs, Response::HTTP_OK);
	}
}
