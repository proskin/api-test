<?php

namespace App\Service;

use App\Entity\Branch;
use App\Exception\ApiDataException;
use App\Exception\ApiDataFetchFailedException;
use Symfony\Contracts\Service\Attribute\Required;

class BranchService {

	private UlozenkaBranchService $ulozenkaBranchService;

	public function __construct(UlozenkaBranchService $ulozenkaBranchService)
	{
		$this->ulozenkaBranchService = $ulozenkaBranchService;
	}

	/**
	 * @return Branch[]
	 */
	public function getBranchs() : array {
		return $this->ulozenkaBranchService->getBranchs();
	}

	/**
	 * @throws \App\Exception\ApiDataFetchFailedException
	 */
	public function getBranchsAsArray() : array
	{
		$branchs = $this->getBranchs();

		$arrayBranch = [];

		/** @var Branch $branch */
		foreach ($branchs as $branch) {
			$arrayBranch[] = $branch->toArray();
		}

		return $arrayBranch;
	}

	/**
	 * @param string $internalId
	 * @return array Branch as array
	 * @throws \App\Exception\ApiDataException
	 */
	public function getBranchAsArray(string $internalId) : array
	{
		$branch = $this->getBranch($internalId);

		return $branch->toArray();

	}

	public function getBranch(string $internalId) : Branch
	{
		$branchs = $this->getBranchs();

		foreach ($branchs as $branch) {
			if($branch->getInternalId() === $internalId) {
				return $branch;
			}
		}

		throw  ApiDataException::notFound('internalId', $internalId);
	}

}