<?php

namespace App\Service;

use App\vo\HttpMethod;
use App\vo\Url;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;

class ApiClient {

	public const NO_TIMEOUT = 0;

	#@return \GuzzleHttp\Client
	protected function createClient(): Client
	{
		return new Client();
	}

	public function get(Url $url, array $payload = [])
	{
		return $this->call(new HttpMethod('GET'), $url, $payload);
	}

	public function call(HttpMethod $method, Url $url, array $payload = []): Response
	{
		$client = $this->createClient();

		$options = [
			'json' => $payload,
			'timeout' => self::NO_TIMEOUT,
		];

		$response = $client->request((string)$method, (string)$url, $options);

		return $response;
	}

	public function getJsonBody(Response $response) : array
	{
		return json_decode($response->getBody());
	}

}

