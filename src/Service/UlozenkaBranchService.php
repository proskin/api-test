<?php

namespace App\Service;

use App\Repository\UlozenkaBranchRepository;
use Symfony\Contracts\Service\Attribute\Required;

class UlozenkaBranchService {

	private UlozenkaBranchRepository $ulozenkaBranchRepository;

	public function __construct(UlozenkaBranchRepository $ulozenkaBranchRepository)
	{
		$this->ulozenkaBranchRepository = $ulozenkaBranchRepository;
	}

	/**
	 * @return \App\Entity\Branch[]
	 * @throws \App\Exception\ApiDataFetchFailedException
	 */
	public function getBranchs() : array
	{
		return $this->ulozenkaBranchRepository->getBranchs();
	}


}