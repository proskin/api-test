<?php

namespace App\Mapper;

use App\Entity\Branch;
use App\Entity\BusinessHour;

class BusinessHourMap {

	/**
	 * @return BusinessHour[]
	 */
	public function listFromJson(array $arrayBussinesHours) : array
	{
		$businesHours = [];

		foreach ($arrayBussinesHours as $bussinesHour) {
			$businesHours[] = $this->oneFromJson($bussinesHour);
		}

		return $businesHours;
	}

	public function oneFromJson(\stdClass $source) : BusinessHour
	{
		$branchHour = new BusinessHour();

		$branchHour->setBusinessHour(
			sprintf('%s - %s',
				$source->open ?? '',
				$source->close ?? ''
			)
		);

		$branchHour->setDayOfWeek($source->day);

		return $branchHour;

	}


}