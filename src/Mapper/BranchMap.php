<?php

namespace App\Mapper;

use App\Entity\Branch;
use App\Mapper\BusinessHourMap;

class BranchMap {

	#[required]
	private BusinessHourMap $businessHourMap;

	public function __construct(BusinessHourMap $businessHourMap)
	{
		$this->businessHourMap = $businessHourMap;
	}

	/**
	 * @return Branch[]
	 */
	public function listFromJson(array $arrayBranchs) : array
	{
		$branchs = [];

		foreach ($arrayBranchs as $branch) {
			$branchs[] = $this->oneFromJson($branch);
		}

		return $branchs;
	}

	public function oneFromJson(\stdClass $source) : Branch
	{
		$branch = new Branch();

		$branch->setInternalId($source->id ?? '');
		$branch->setInternalName($source->shortcut ?? '');
		$branch->setAddress($source->name ?? '');
		$branch->setWeb($source->odkaz ?? '');
		$branch->setAnnouncement($source->announcements ?? []);
		$branch->setLocation($this->mapLocation($source));
		$branch->setBusinessHours(
			$this->businessHourMap->listFromJson($source->openingHours)
		);

		return $branch;
	}

	private function mapLocation(\stdClass $source) : string
	{
		return sprintf('%d, %d',
			$source->lat ?? '',
			$source->lng ?? ''
		);
	}


}