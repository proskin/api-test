<?php

namespace App\vo;

final class Url
{

	/**
	 * @var string
	 */
	private $url;

	/**
	 * @param string $url
	 * @param array<int, string> $params
	 * @throws \DomainException
	 */
	public function __construct(string $url, ...$params)
	{
		$url = \sprintf($url, ...$params);

		if (\filter_var($url, \FILTER_VALIDATE_URL) === FALSE) {
			throw new \DomainException('Url is not valid');
		}

		$this->url = $url;
	}

	/**
	 * @param string $basePath
	 * @param string $path
	 * @return \Drmax\VO\Url
	 */
	public static function compose(string $basePath, string $path): self
	{
		return new self($basePath . $path);
	}

	/**
	 * @return string
	 */
	public function __toString(): string
	{
		return $this->url;
	}

}
