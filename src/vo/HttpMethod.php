<?php declare(strict_types=1);

namespace App\vo;

final class HttpMethod
{

	/**
	 * @var string
	 */
	private $method;

	/**
	 * Only digestible HTTP methods for rabbitmq api
	 */
	public const ALLOWED = ['GET', 'POST', 'PUT', 'PATCH', 'DELETE'];

	/**
	 * @param string $method
	 * @throws \DomainException
	 */
	public function __construct(string $method)
	{
		/**
		 * To have consistent method names
		 */
		$method = \strtoupper($method);

		if (!\in_array($method, self::ALLOWED, TRUE)) {
			throw new \DomainException(
				\sprintf('Method can only be %s', \implode(', ', self::ALLOWED))
			);
		}

		$this->method = $method;
	}

	/**
	 * @return string
	 */
	public function __toString(): string
	{
		return $this->method;
	}

}
