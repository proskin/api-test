<?php

namespace App\Exception;

class ApiDataFetchFailedException extends \Exception
{

	public function __construct(string $message, string $endpoint, int $code)
	{
		parent::__construct(
			sprintf('Fetch data from %s failed: %s', $endpoint, $message),
			$code
		);
	}
}