<?php

namespace App\Entity;

use App\Entity\BusinessHour;

class Branch
{

	private string $internalId;

	private string $internalName;

	private string $location;

	# @var array<BusinessHour>
	private array $businessHours;

	private string $address;

	private string $web;

	private array $announcement;

	/**
	 * @return string
	 */
	public function getInternalId(): string
	{
		return $this->internalId;
	}

	/**
	 * @param string $internalId
	 */
	public function setInternalId(string $internalId): void
	{
		$this->internalId = $internalId;
	}

	/**
	 * @return string
	 */
	public function getInternalName(): string
	{
		return $this->internalName;
	}

	/**
	 * @param string $internalName
	 */
	public function setInternalName(string $internalName): void
	{
		$this->internalName = $internalName;
	}

	/**
	 * @return string
	 */
	public function getLocation(): string
	{
		return $this->location;
	}

	/**
	 * @param string $location
	 */
	public function setLocation(string $location): void
	{
		$this->location = $location;
	}

	/**
	 * @return \App\Entity\BusinessHour[]
	 */
	public function getBusinessHours(): array
	{
		return $this->businessHours;
	}

	/**
	 * @param \App\Entity\BusinessHour[] $businessHours
	 */
	public function setBusinessHours(array$businessHours): void
	{
		$this->businessHours = $businessHours;
	}

	/**
	 * @return String
	 */
	public function getAddress(): string
	{
		return $this->address;
	}

	/**
	 * @param String $address
	 */
	public function setAddress(string $address): void
	{
		$this->address = $address;
	}

	/**
	 * @return String
	 */
	public function getWeb(): string
	{
		return $this->web;
	}

	/**
	 * @param String $web
	 */
	public function setWeb(string $web): void
	{
		$this->web = $web;
	}

	/**
	 * @return String
	 */
	public function getAnnouncement(): array
	{
		return $this->announcement;
	}

	/**
	 * @param array $announcement
	 */
	public function setAnnouncement(array $announcement): void
	{
		$this->announcement = $announcement;
	}

	public function toArray() : array
	{
		$array =  [
			'internalId' => $this->getInternalId(),
			'internalName' => $this->getInternalName(),
			'address' => $this->getAddress(),
		];

		/** @var BusinessHour $businessHour */
		foreach ($this->getBusinessHours() as $businessHour) {
			$array['businessHours'][] = $businessHour->toArray();
		}

		return $array;
	}




}