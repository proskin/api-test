<?php

namespace App\Entity;

class BusinessHour
{
	private string $dayOfWeek;

	private string $businessHour;

	/**
	 * @return string
	 */
	public function getDayOfWeek(): string
	{
		return $this->dayOfWeek;
	}

	/**
	 * @param string $dayOfWeek
	 */
	public function setDayOfWeek(string $dayOfWeek): void
	{
		$this->dayOfWeek = $dayOfWeek;
	}

	/**
	 * @return string
	 */
	public function getBusinessHour(): string
	{
		return $this->businessHour;
	}

	/**
	 * @param string $businessHour
	 */
	public function setBusinessHour(string $businessHour): void
	{
		$this->businessHour = $businessHour;
	}

	public function toArray() : array {
		return [
			'dayOfWeek' => $this->getDayOfWeek(),
			'businessHour' => $this->getBusinessHour()
		];
	}


}