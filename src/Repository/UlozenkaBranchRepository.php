<?php

namespace App\Repository;

use App\Exception\ApiDataFetchFailedException;
use App\Mapper\BranchMap;
use App\Service\ApiClient;
use App\vo\Url;


class UlozenkaBranchRepository {

	#[required]
	private ApiClient $apiClient;

	private Url $apiBranchPath;

	private BranchMap $branchMap;

	public function __construct(ApiClient $apiClient, string $apiBranchPath, BranchMap $branchMap)
	{
		$this->apiClient = $apiClient;
		$this->apiBranchPath = new Url($apiBranchPath);
		$this->branchMap = $branchMap;
	}

	public function getBranchs()
	{
		$response = $this->apiClient->get($this->apiBranchPath);

		if($response->getStatusCode() === 200) {
			return $this->branchMap->listFromJson(
				$this->apiClient->getJsonBody($response)
			);
		}

		throw new ApiDataFetchFailedException($response->getReasonPhrase(), $this->apiBranchPath, $response->getStatusCode());

	}
}